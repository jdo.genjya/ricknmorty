package tt.eugene

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import tt.eugene.di.characterDataModule
import tt.eugene.di.databaseModule
import tt.eugene.di.networkModule
import tt.eugene.di.viewModelModule

class RicknMortyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@RicknMortyApp)
            modules(
                viewModelModule,
                networkModule,
                characterDataModule,
                databaseModule)
        }
    }
}