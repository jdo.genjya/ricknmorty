package tt.eugene.data.database

import androidx.room.TypeConverter
import kotlinx.datetime.Instant

object Converters {

    @TypeConverter
    fun fromInstant(instant: Instant): Long {
        return instant.toEpochMilliseconds()
    }

    @TypeConverter
    fun toInstant(value: Long): Instant {
        return Instant.fromEpochMilliseconds(value)
    }
}