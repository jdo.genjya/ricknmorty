package tt.eugene.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import tt.eugene.data.database.dao.CharacterDao

@Database(entities = [CharacterEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class Database : RoomDatabase() {
    abstract fun characterDao(): CharacterDao
}