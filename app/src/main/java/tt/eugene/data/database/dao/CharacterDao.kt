package tt.eugene.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import tt.eugene.data.database.CharacterEntity

@Dao
interface CharacterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<CharacterEntity>)

    @Query("SELECT * FROM characters WHERE id = :id")
    suspend fun getById(id: Int): CharacterEntity?


    @Query("SELECT * FROM characters ORDER BY id LIMIT :limit OFFSET :offset")
    suspend fun getWithOffsetAndLimit(
        offset: Int,
        limit: Int,
    ): List<CharacterEntity>

    @Query("DELETE FROM characters")
    suspend fun clearAll()
}

suspend fun CharacterDao.getPaged(
    page: Int,
    pageSize: Int,
): List<CharacterEntity> {
    val offset = maxOf(page - 1, 1) * pageSize
    println("Query offset $offset limit $pageSize")
    return getWithOffsetAndLimit(offset, pageSize)
}