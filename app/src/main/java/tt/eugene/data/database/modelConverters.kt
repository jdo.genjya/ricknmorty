package tt.eugene.data.database

import tt.eugene.sharedModels.Character
import tt.eugene.sharedModels.Url

fun Character.toEntity(): CharacterEntity {
    return CharacterEntity(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        origin = origin,
        location = location,
        image = image.value,
        created = created,
    )
}

fun CharacterEntity.toModel(): Character {
    return Character(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        origin = origin,
        location = location,
        image = Url(image),
        created = created,
    )
}