package tt.eugene.data.network

import retrofit2.http.GET
import retrofit2.http.Query
import tt.eugene.data.network.response.CharacterResponse
import tt.eugene.data.network.response.PagedResponse

interface RicknMortyApi {

    @GET("character")
    suspend fun getCharacters(@Query("page") page: Int): PagedResponse<CharacterResponse>
}