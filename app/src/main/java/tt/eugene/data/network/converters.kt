package tt.eugene.data.network

import tt.eugene.data.network.response.CharacterResponse
import tt.eugene.sharedModels.Character

fun CharacterResponse.toCharacter(): Character {
    return Character(
        id = id,
        name = name,
        status = status,
        species = species,
        type = type,
        gender = gender,
        origin = origin.name,
        location = location.name,
        image = image,
        created = created,
    )
}