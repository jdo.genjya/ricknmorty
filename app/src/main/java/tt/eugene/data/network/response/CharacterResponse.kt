package tt.eugene.data.network.response

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import tt.eugene.sharedModels.Url

@Serializable
data class CharacterResponse(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: Origin,
    val location: Location,
    val image: Url,
    val episode: List<Url>,
    val url: Url,
    val created: Instant,
)

@Serializable
data class Origin(
    val name: String,
    val url: Url,
)

@Serializable
data class Location(
    val name: String,
    val url: Url,
)