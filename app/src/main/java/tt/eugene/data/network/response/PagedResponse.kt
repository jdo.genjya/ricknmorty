package tt.eugene.data.network.response

import kotlinx.serialization.Serializable
import tt.eugene.sharedModels.Url

@Serializable
data class PagedResponse<T>(
    val info: PageInfo,
    val results: List<T>,
)

@Serializable
data class PageInfo(
    val count: Int,
    val pages: Int,
    val next: Url?,
    val prev: Url?,
)