package tt.eugene.data.pagination

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import okhttp3.HttpUrl.Companion.toHttpUrl
import retrofit2.HttpException
import tt.eugene.data.database.Database
import tt.eugene.data.database.dao.getPaged
import tt.eugene.data.database.toEntity
import tt.eugene.data.database.toModel
import tt.eugene.data.network.RicknMortyApi
import tt.eugene.data.network.response.CharacterResponse
import tt.eugene.data.network.response.PagedResponse
import tt.eugene.data.network.toCharacter
import tt.eugene.sharedModels.Character
import tt.eugene.sharedModels.Url
import java.io.IOException
import kotlin.coroutines.cancellation.CancellationException

private const val TAG = "CharacterPagingSource"

class CharacterPagingSource(
    private val service: RicknMortyApi,
    database: Database,
    private val pageSize: Int = 20,
) : PagingSource<Int, Character>() {

    private val charactersDao = database.characterDao()

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Character> {
        try {
            val nextPage = params.key ?: 1
            Log.d(TAG, "Request page: $nextPage")
            val characters = charactersDao.getPaged(nextPage, pageSize)
                .map { it.toModel() }
            if (characters.isNotEmpty()) {
                Log.d(TAG, "Get from DB: ${characters.first().id}..${characters.last().id}")
            }
            return when {
                characters.size < pageSize -> getFreshPageIfPossible(nextPage, characters)
                characters.isNotEmpty() -> emitCachedPage(nextPage, characters)
                else -> loadPage(nextPage)
            }
        } catch (cancellation: CancellationException) {
            // rethrow to propagate cancellation
            throw cancellation
        } catch (exception: IOException) {
            // IOException for network failures.
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            // HttpException for any non-2xx HTTP status codes.
            return LoadResult.Error(exception)
        } catch (exception: Exception) {
            exception.printStackTrace()
            return LoadResult.Invalid()
        }

    }

    private suspend fun CharacterPagingSource.loadPage(
        nextPage: Int
    ): LoadResult.Page<Int, Character> {
        val response = service.getCharacters(nextPage)
        val updatedCharacters = insertResponse(response, nextPage)
        val prev = response.info.prev?.let(::pageFromUrl)
        val next = response.info.next?.let(::pageFromUrl)
        Log.d(TAG, "Return data: prev=$prev next=$next")
        return LoadResult.Page(
            updatedCharacters,
            prev,
            next,
        )
    }

    private suspend fun CharacterPagingSource.getFreshPageIfPossible(
        nextPage: Int,
        characters: List<Character>
    ) = try {
        val response = service.getCharacters(nextPage)
        val updatedCharacters = insertResponse(response, nextPage)
        val prev = response.info.prev?.let(::pageFromUrl)
        val next = response.info.next?.let(::pageFromUrl)
        Log.d(TAG, "Return data: prev=$prev next=$next")
        LoadResult.Page(
            updatedCharacters,
            prev,
            next,
        )
    } catch (e: Exception) {
        emitCachedPage(nextPage, characters)
    }

    private fun emitCachedPage(
        nextPage: Int,
        characters: List<Character>
    ): LoadResult.Page<Int, Character> {
        val prev = if (nextPage == 1) null else nextPage - 1
        val next = nextPage + 1
        Log.d(TAG, "Return data: prev=$prev next=$next")
        return LoadResult.Page(
            characters,
            prev,
            next,
        )
    }

    override fun getRefreshKey(state: PagingState<Int, Character>): Int? {
        // Try to find the page key of the closest page to anchorPosition from
        // either the prevKey or the nextKey; you need to handle nullability
        // here.
        //  * prevKey == null -> anchorPage is the first page.
        //  * nextKey == null -> anchorPage is the last page.
        //  * both prevKey and nextKey are null -> anchorPage is the
        //    initial page, so return null.
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }

    }

    private suspend fun insertResponse(
        response: PagedResponse<CharacterResponse>,
        nextPage: Int
    ): List<Character> {
        val items = response.results.map { it.toCharacter().toEntity() }
        Log.d(TAG, "Fetched: ${items.first().id}..${items.last().id}")
        charactersDao.insertAll(items)
        val updated = charactersDao.getPaged(nextPage, pageSize)
            .map { it.toModel() }
        Log.d(
            TAG,
            "Get from DB fetched: ${updated.first().id}..${updated.last().id}"
        )
        return updated
    }
}

private fun pageFromUrl(url: Url): Int? {
    return url.value.toHttpUrl().queryParameter("page")?.toInt()
}