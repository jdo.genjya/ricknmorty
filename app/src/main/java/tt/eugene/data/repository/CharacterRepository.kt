package tt.eugene.data.repository

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import tt.eugene.sharedModels.Character

interface CharacterRepository {
    fun getAll(): Flow<PagingData<Character>>
    suspend fun getCharacter(characterId: Int): Character?
}