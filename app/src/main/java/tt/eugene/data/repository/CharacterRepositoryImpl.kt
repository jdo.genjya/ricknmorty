package tt.eugene.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import tt.eugene.data.database.Database
import tt.eugene.data.database.toModel
import tt.eugene.data.network.RicknMortyApi
import tt.eugene.data.pagination.CharacterPagingSource
import tt.eugene.sharedModels.Character

private const val PAGE_SIZE = 20

class CharacterRepositoryImpl(
    private val dataSource: RicknMortyApi,
    private val database: Database,
) : CharacterRepository {

    override fun getAll(): Flow<PagingData<Character>> {
        return Pager(
            PagingConfig(pageSize = PAGE_SIZE, enablePlaceholders = true),
            pagingSourceFactory = {
                CharacterPagingSource(dataSource, database, PAGE_SIZE)
            },
        ).flow
    }

    override suspend fun getCharacter(characterId: Int): Character? {
        return database.characterDao().getById(id = characterId)?.toModel()
    }
}