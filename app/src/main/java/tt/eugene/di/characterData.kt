package tt.eugene.di

import org.koin.dsl.module
import tt.eugene.data.repository.CharacterRepository
import tt.eugene.data.repository.CharacterRepositoryImpl

val characterDataModule = module {
    factory<CharacterRepository> { CharacterRepositoryImpl(get(), get()) }
}