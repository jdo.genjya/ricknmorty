package tt.eugene.di

import androidx.room.Room
import org.koin.dsl.module
import tt.eugene.data.database.Database

val databaseModule = module {
    single {
        Room.databaseBuilder(
            get(),
            Database::class.java, "rick-and-morty-database"
        ).build()

    }
}