package tt.eugene.di

import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.kotlinx.serialization.asConverterFactory
import retrofit2.create
import tt.eugene.data.network.RicknMortyApi

private val json = Json {
    ignoreUnknownKeys = true
}

val networkModule = module {
    single {
        OkHttpClient.Builder()
            .build()
    }
    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl("https://rickandmortyapi.com/api/")
            .addConverterFactory(
                json.asConverterFactory(
                    "application/json; charset=UTF8".toMediaType()
                )
            )
            .build()
    }

    single {
        get<Retrofit>().create<RicknMortyApi>()
    }
}