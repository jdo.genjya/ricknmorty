package tt.eugene.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module
import tt.eugene.ui.characterDetail.CharacterDetailsViewModel
import tt.eugene.ui.characters.CharactersViewModel

val viewModelModule = module {
    viewModelOf(::CharacterDetailsViewModel)
    viewModel {CharactersViewModel(get())}
}