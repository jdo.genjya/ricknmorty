package tt.eugene.sharedModels

import kotlinx.serialization.Serializable
import java.time.Instant

@Serializable
data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: String,
    val location: String,
    val image: Url,
    val created: kotlinx.datetime.Instant,
)

@Serializable
data class Origin(
    val name: String,
    val url: Url,
)

@Serializable
data class Location(
    val name: String,
    val url: Url,
)

@Serializable
@JvmInline
value class Url(val value: String)