package tt.eugene.ui.characterDetail

import androidx.compose.runtime.Stable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import tt.eugene.data.repository.CharacterRepository
import tt.eugene.sharedModels.Character
import tt.eugene.ui.characters.CharactersSideEffect

class CharacterDetailsViewModel(private val repository: CharacterRepository) : ViewModel() {
    private val _state: MutableStateFlow<CharacterDetailsState> = MutableStateFlow(
        CharacterDetailsState(false, null)
    )

    val state: StateFlow<CharacterDetailsState> = _state.asStateFlow()

    private val sideEffectChannel = Channel<CharacterDetailsSideEffect>(Channel.CONFLATED)

    private val sideEffects: SharedFlow<CharacterDetailsSideEffect> = sideEffectChannel
        .consumeAsFlow()
        .shareIn(
            scope = viewModelScope,
            started = SharingStarted.Eagerly,
            replay = 0
        )

    val navSideEffects = sideEffects
        .filterIsInstance<CharacterDetailsSideEffect.Nav>()

    fun getCharacter(itemId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val character = repository.getCharacter(itemId)
            _state.update { old -> old.copy(loading = false, item = character) }
        }
    }

}

sealed interface CharacterDetailsSideEffect {
    sealed interface Nav : CharacterDetailsSideEffect {
        data object NavBack : Nav
        data object None : Nav
    }
}

@Stable
data class CharacterDetailsState(
    val loading: Boolean,
    val item: Character?
)