package tt.eugene.ui.characters

import androidx.compose.runtime.Stable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import tt.eugene.data.repository.CharacterRepository
import tt.eugene.sharedModels.Character

class CharactersViewModel(private val repository: CharacterRepository) : ViewModel() {

    init {
        obtainAllCharacters()
    }

    private val _state: MutableStateFlow<CharactersState> = MutableStateFlow(
        CharactersState(false, emptyFlow())
    )

    val state: StateFlow<CharactersState> = _state.asStateFlow()

    private val sideEffectChannel = Channel<CharactersSideEffect>(Channel.CONFLATED)

    private val sideEffects: SharedFlow<CharactersSideEffect> = sideEffectChannel
        .consumeAsFlow()
        .shareIn(
            scope = viewModelScope,
            started = SharingStarted.Eagerly,
            replay = 0
        )

    val navSideEffects = sideEffects
        .filterIsInstance<CharactersSideEffect.Nav>()

    private fun obtainAllCharacters() {
        viewModelScope.launch(Dispatchers.IO) {
            val characters = repository.getAll()
            _state.update { old -> old.copy(loading = false, items = characters) }
        }
    }

    fun onItemClicked(id: Int) {
        viewModelScope.launch {
            sideEffectChannel.send(CharactersSideEffect.Nav.NavToDetails(id))
        }
    }

}

sealed interface CharactersSideEffect {
    sealed interface Nav : CharactersSideEffect {
        data class NavToDetails(val id: Int) : Nav
        data object None : Nav
    }
}

@Stable
data class CharactersState(
    val loading: Boolean,
    val items: Flow<PagingData<Character>>
)