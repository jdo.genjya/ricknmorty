package tt.eugene.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import org.koin.androidx.compose.koinViewModel
import tt.eugene.ui.characterDetail.CharacterDetailsScreen
import tt.eugene.ui.characterDetail.CharacterDetailsSideEffect
import tt.eugene.ui.characterDetail.CharacterDetailsViewModel
import tt.eugene.ui.characters.CharactersScreen
import tt.eugene.ui.characters.CharactersSideEffect
import tt.eugene.ui.characters.CharactersViewModel

sealed class Screen(val route: String) {
    data object Characters : Screen("characters")
    data object CharacterDetails : Screen("characterDetails/{itemId}") {
        fun createRoute(itemId: Int) = "characterDetails/$itemId"
    }
}


@Composable
fun AppNavigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screen.Characters.route) {

        composable(Screen.Characters.route) {
            val viewModel: CharactersViewModel = koinViewModel()

            val state by viewModel.state.collectAsStateWithLifecycle(
                lifecycleOwner = androidx.compose.ui.platform.LocalLifecycleOwner.current
            )
            val nav by
            viewModel.navSideEffects.collectAsStateWithLifecycle(
                lifecycleOwner = androidx.compose.ui.platform.LocalLifecycleOwner.current,
                initialValue = CharactersSideEffect.Nav.None)

            LaunchedEffect(key1 = nav) {
                when (nav) {
                    is CharactersSideEffect.Nav.NavToDetails -> {
                        navController.navigate(Screen.CharacterDetails.createRoute(itemId = (nav as CharactersSideEffect.Nav.NavToDetails).id))
                    }

                    CharactersSideEffect.Nav.None -> {}
                }
            }

            CharactersScreen(state, onItemClick = viewModel::onItemClicked)

        }

        composable(route = Screen.CharacterDetails.route, arguments = listOf(navArgument("itemId") { type = NavType.IntType })) { backStackEntry ->

            val itemId = backStackEntry.arguments?.getInt("itemId")

            val viewModel: CharacterDetailsViewModel = koinViewModel()
            val state by viewModel.state.collectAsStateWithLifecycle(
                lifecycleOwner = androidx.compose.ui.platform.LocalLifecycleOwner.current
            )

            itemId?.let { viewModel.getCharacter(itemId) }

            val nav by viewModel.navSideEffects.collectAsStateWithLifecycle(
                lifecycleOwner = androidx.compose.ui.platform.LocalLifecycleOwner.current,
                initialValue = CharacterDetailsSideEffect.Nav.None)

            LaunchedEffect(key1 = nav) {
                when (nav) {
                    CharacterDetailsSideEffect.Nav.NavBack -> {
                        navController.navigateUp()
                    }

                    CharacterDetailsSideEffect.Nav.None -> {}

                }
            }

            CharacterDetailsScreen(state)
        }
    }
}



